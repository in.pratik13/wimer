package com.vyantech.wimer.frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.vyantech.wimer.label.TimeDisplayLabel;
import com.vyantech.wimer.listeners.AbstractWindowListener;
import com.vyantech.wimer.stopwatch.StopWatch;
import com.vyantech.wimer.stopwatch.StopWatchListener;
import com.vyantech.wimer.tick.TickListener;
import com.vyantech.wimer.tick.TickProvider;

public class MainFrame extends JFrame implements StopWatchListener, TickListener {

	private static final int LAYOUT_GAP = 10;
	private static final Color COLOR_BLUE = new Color(33, 150, 243);
	private static final Color COLOR_GRAY = new Color(49, 60, 75);
	private static final long serialVersionUID = -3250465181368972859L;

	private final JButton startButton = new JButton();
	private final JButton resetButton = new JButton("RESET");
	private final StopWatch watch;
	private final JLabel timeLabel;

	public MainFrame(StopWatch watch, TickProvider tickProvider) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300, 300);
		setResizable(false);

		this.watch = watch;
		this.timeLabel = new TimeDisplayLabel(watch, Color.WHITE, COLOR_BLUE);

		addWindowListener(new AbstractWindowListener() {
			@Override
			public void windowClosing(WindowEvent e) {
				tickProvider.stop();
			}
		});

		setupElements();
		this.watch.addStatusListener(this);
		tickProvider.addListener(this);
	}

	@Override
	public void reset() {
		showDuration();
		updateButton();
		resetButton.setEnabled(false);
	}

	@Override
	public void started() {
		showDuration();
		updateButton();
		resetButton.setEnabled(true);
	}

	@Override
	public void stopped() {
		showDuration();
		updateButton();
		resetButton.setEnabled(true);
	}

	@Override
	public void tick() {
		showDuration();
	}

	private void setupElements() {
		setLayout(new BorderLayout(LAYOUT_GAP, LAYOUT_GAP));

		showDuration();
		updateButton();
		setWhiteFontColor(timeLabel);

		Container container = getContentPane();
		container.setBackground(COLOR_GRAY);
		addTimeLabel(container);
		addButtons(container);
	}

	private void addTimeLabel(Container container) {
		Container timeContainer = new Container();
		timeContainer.setLayout(new GridBagLayout());
		timeContainer.add(timeLabel, new GridBagConstraints());

		container.add(timeContainer, BorderLayout.CENTER);
	}

	private void showDuration() {
		timeLabel.setText(watch.elapsedDisplayString());
	}

	private void updateButton() {
		startButton.setText(getPlayText());
	}

	private static void setButtonUI(JButton button) {
		button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		button.setFont(new Font("Tahoma", Font.BOLD, 14));
		button.setForeground(COLOR_BLUE);
		// button.setForeground(Color.WHITE);
		// button.setBackground(new Color(33, 150, 243));
		// button.setOpaque(true);
		button.setBorderPainted(false);
	}

	private static void setWhiteFontColor(JComponent label) {
		label.setFont(new Font("Tahoma", Font.PLAIN, 32));
		label.setForeground(Color.WHITE);
	}

	private void addButtons(Container container) {
		FlowLayout btnLayout = new FlowLayout(FlowLayout.CENTER);
		Container btnContainer = new Container();
		btnContainer.setLayout(btnLayout);
		container.add(btnContainer, BorderLayout.PAGE_END);

		setButtonUI(startButton);
		btnContainer.add(startButton);

		setButtonUI(resetButton);
		resetButton.setEnabled(false);
		btnContainer.add(resetButton);

		startButton.addActionListener(e -> watch.toggleStartStop());
		resetButton.addActionListener(e -> watch.reset());
	}

	private String getPlayText() {
		return watch.isRunning() ? "STOP" : "START";
	}
}
