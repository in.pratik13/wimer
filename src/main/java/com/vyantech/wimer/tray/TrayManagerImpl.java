package com.vyantech.wimer.tray;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.vyantech.wimer.listeners.AbstractMounseListener;
import com.vyantech.wimer.stopwatch.StopWatch;
import com.vyantech.wimer.stopwatch.StopWatchListener;
import com.vyantech.wimer.tick.TickListener;
import com.vyantech.wimer.tick.TickProvider;

public class TrayManagerImpl extends AbstractMounseListener implements TrayManager, StopWatchListener, TickListener {
	private final StopWatch watch;
	private final TrayIcon trayIcon;
	private final MenuItem timeMenu;
	private final MenuItem pauseItem;
	private final Image playImage, stopImage;
	
	public TrayManagerImpl(StopWatch watch, TickProvider tickProvider) {
		SystemTray tray = SystemTray.getSystemTray();
		playImage = getImage("/assets/w_play.png");
		stopImage = getImage("/assets/w_stop.png");

		PopupMenu popup = new PopupMenu();
		timeMenu = new MenuItem();
		timeMenu.setEnabled(false);
		popup.add(timeMenu);

		pauseItem = new MenuItem();
		popup.add(pauseItem);
		pauseItem.addActionListener(e -> watch.toggleStartStop());

		trayIcon = new TrayIcon(stopImage);
		trayIcon.setPopupMenu(popup);
		trayIcon.setImageAutoSize(true);
		trayIcon.addMouseListener(this);

		try {
			tray.add(trayIcon);
		} catch (AWTException e) {
			e.printStackTrace();
		}

		this.watch = watch;
		updateMenuLabels();
		watch.addStatusListener(this);
		tickProvider.addListener(this);
	}

	private BufferedImage getImage(String path) {
		try {
			return ImageIO.read(getClass().getResource(path));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		updateMenuLabels();
	}

	@Override
	public void stopped() {
		updateMenuLabels();
		trayIcon.setImage(stopImage);
		
	}

	@Override
	public void started() {
		updateMenuLabels();
		trayIcon.setImage(playImage);
	}

	@Override
	public void reset() {
		updateMenuLabels();
	}

	@Override
	public void tick() {
		updateMenuLabels();
	}

	private void updateMenuLabels() {
		timeMenu.setLabel(watch.elapsedDisplayString());
		pauseItem.setLabel(getPlayText());
	}

	private String getPlayText() {
		return watch.isRunning() ? "STOP" : "START";
	}
}
