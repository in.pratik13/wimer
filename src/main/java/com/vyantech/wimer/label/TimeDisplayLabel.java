package com.vyantech.wimer.label;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.TimeUnit;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import com.vyantech.wimer.stopwatch.StopWatch;

public class TimeDisplayLabel extends JLabel implements MouseListener {

	private static final long serialVersionUID = -2821973364813465411L;
	private final StopWatch watch;
	private final Color normalColor, highlightedColor;

	public TimeDisplayLabel(StopWatch watch, Color normalColor, Color highlightedColor) {
		super("", SwingConstants.CENTER);
		
		this.watch = watch;
		this.normalColor = normalColor;
		this.highlightedColor = highlightedColor;

		this.setForeground(normalColor);
		this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		this.addMouseListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		String minStr = JOptionPane.showInputDialog(null, "Input new time to set. (in minutes)",
				watch.elapsed(TimeUnit.MINUTES));
		if (minStr == null) {
			return;
		}

		try {
			final int min = Integer.parseInt(minStr);
			watch.setTime(min);
		} catch (NullPointerException | NumberFormatException ne) {
			JOptionPane.showMessageDialog(this,
					"Invalid minutes entered.\n" + ne.getMessage(),
					"Input Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		this.setForeground(highlightedColor);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		this.setForeground(normalColor);
	}
}
