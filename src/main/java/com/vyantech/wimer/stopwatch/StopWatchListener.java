package com.vyantech.wimer.stopwatch;

public interface StopWatchListener {
	public void stopped();
	public void started();
	public void reset();
}
